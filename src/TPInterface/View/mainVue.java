package TPInterface.View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.SpringLayout;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JFormattedTextField;
import javax.swing.JList;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class mainVue {

	JFrame frame;
	private JTextField txtNomFamille;
	private JTextField txtPrixBillet;
	private JLabel lblSiegesReserves;
	private JLabel lblSigesRestants;
	private JLabel lblNbTotaux;
	private JLabel lblNbReserves;
	private JLabel lblNbRestants;
	private JRadioButton rdbtnPremier = new JRadioButton("Repr\u00E9sentation semaine courante");
	private JRadioButton rdbtnDeuxieme = new JRadioButton("Repr\u00E9sentation semaine prochaine");
	private JLabel lblNomFamille = new JLabel("Nom de Famille du Client");
	private JLabel lblPrixDuBillet = new JLabel("Prix du Billet");
	private JButton btnAjoutClient = new JButton("Ajouter le client");
	private JButton btnAfficherLesRservations = new JButton("Afficher les r\u00E9servations");
	private JLabel lblSiegesTotaux;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mainVue window = new mainVue();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public mainVue() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 670, 433);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		rdbtnPremier.setBounds(25, 28, 229, 23);
		frame.getContentPane().add(rdbtnPremier);
		
		rdbtnDeuxieme.setBounds(25, 110, 229, 23);
		frame.getContentPane().add(rdbtnDeuxieme);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnPremier);
		group.add(rdbtnDeuxieme);
		
		txtNomFamille = new JTextField();
		txtNomFamille.setBounds(25, 221, 187, 20);
		frame.getContentPane().add(txtNomFamille);
		txtNomFamille.setColumns(10);
		
		lblNomFamille.setHorizontalAlignment(SwingConstants.CENTER);
		lblNomFamille.setBounds(25, 196, 187, 14);
		frame.getContentPane().add(lblNomFamille);
		
		lblPrixDuBillet.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrixDuBillet.setBounds(25, 269, 187, 14);
		frame.getContentPane().add(lblPrixDuBillet);
		
		txtPrixBillet = new JTextField();
		txtPrixBillet.setColumns(10);
		txtPrixBillet.setBounds(25, 294, 187, 20);
		frame.getContentPane().add(txtPrixBillet);
		
		btnAjoutClient.setBounds(25, 350, 187, 23);
		frame.getContentPane().add(btnAjoutClient);
		
		lblSiegesTotaux = new JLabel("Si\u00E8ges Totaux : ");
		lblSiegesTotaux.setHorizontalAlignment(SwingConstants.CENTER);
		lblSiegesTotaux.setBounds(305, 28, 115, 37);
		frame.getContentPane().add(lblSiegesTotaux);
		
		lblSiegesReserves = new JLabel("Si\u00E8ges r\u00E9serv\u00E9s  : ");
		lblSiegesReserves.setHorizontalAlignment(SwingConstants.CENTER);
		lblSiegesReserves.setBounds(305, 146, 115, 37);
		frame.getContentPane().add(lblSiegesReserves);
		
		lblSigesRestants = new JLabel("Si\u00E8ges restants : ");
		lblSigesRestants.setHorizontalAlignment(SwingConstants.CENTER);
		lblSigesRestants.setBounds(305, 269, 115, 37);
		frame.getContentPane().add(lblSigesRestants);
		
		lblNbTotaux = new JLabel("Nombre si\u00E8ges totaux");
		lblNbTotaux.setHorizontalAlignment(SwingConstants.CENTER);
		lblNbTotaux.setBounds(463, 28, 115, 37);
		frame.getContentPane().add(lblNbTotaux);
		
		lblNbReserves = new JLabel("Nombre si\u00E8ges r\u00E9serv\u00E9s");
		lblNbReserves.setHorizontalAlignment(SwingConstants.CENTER);
		lblNbReserves.setBounds(463, 146, 115, 37);
		frame.getContentPane().add(lblNbReserves);
		
		lblNbRestants = new JLabel("Nombre si\u00E8ges restants");
		lblNbRestants.setHorizontalAlignment(SwingConstants.CENTER);
		lblNbRestants.setBounds(463, 269, 115, 37);
		frame.getContentPane().add(lblNbRestants);
		
		btnAfficherLesRservations.setBounds(315, 350, 263, 23);
		frame.getContentPane().add(btnAfficherLesRservations);
	}
	
	void addRadioButtonListener(ActionListener listenRadioButton) {
		this.rdbtnPremier.addActionListener(listenRadioButton);
		this.rdbtnDeuxieme.addActionListener(listenRadioButton);
	}
	
	void addBtnAjouter(ActionListener listenAddBtn) {
		this.btnAjoutClient.addActionListener(listenAddBtn);
	}
	
	void BtnAfficherClient(ActionListener listenAfficherBtn) {
		this.btnAfficherLesRservations.addActionListener(listenAfficherBtn);	
	}
	
	void ajouterClientListener(ActionListener listenBtnAjouterClient) {
		this.btnAjoutClient.addActionListener(listenBtnAjouterClient);
	}
	
	void afficherListeClient(ActionListener listenBtnAfficher) {
		this.btnAfficherLesRservations.addActionListener(listenBtnAfficher);
	}
	
	void setNbRestants(int NbRestant) {
		this.lblNbRestants.setText(String.valueOf(NbRestant));
	}
	
	void setNbReserves(int NbReserver) {
		this.lblNbReserves.setText(String.valueOf(NbReserver));
	}
	
	void setNbTotaux(int NbTotaux) {
		this.lblNbTotaux.setText(String.valueOf(NbTotaux));
	}
	
	String getClientName() {
		
		return this.txtNomFamille.getText();
	}
	
	String getPrixBillet() {
		
		return this.txtPrixBillet.getText();
	}
	
	boolean getRadioState() {
		boolean courant = true;
		
		if (this.rdbtnDeuxieme.isSelected()) {
			courant = false;
		}
		
		return courant;
	}
	
}
