package TPInterface.View;

import TPInterface.Model.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class controller {
	Reservations reservation;
	mainVue mainVue;
	vueReservation vueReservation;
	
	public controller(Reservations reservation, mainVue mainVue, vueReservation vueReservation) {
		this.reservation = reservation;
		this.mainVue = mainVue;
		this.vueReservation = vueReservation;
		
		this.mainVue.addRadioButtonListener(new RadioButtonListener());
		this.mainVue.addBtnAjouter(new AjouterButtonListener());
		this.mainVue.afficherListeClient(new AfficherListe());
		//this.vueReservation.btnRevenirReserv(new )
	}
	
	private class RadioButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			reservation.semaineCourante = mainVue.getRadioState();
			
			if (reservation.semaineCourante) {
				mainVue.setNbReserves(reservation.getSiegesReserver());
				mainVue.setNbRestants(reservation.getSiegesLibres());
				mainVue.setNbTotaux(reservation.getSiegesTotaux());
			} else {
				mainVue.setNbReserves(reservation.getSiegesReserverProchaine());
				mainVue.setNbRestants(reservation.getSiegesLibresProchaine());
				mainVue.setNbTotaux(reservation.getSiegesTotaux());
			}
		}
	}
	
	private class AjouterButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			reservation.semaineCourante = mainVue.getRadioState();
			
			if (reservation.semaineCourante) {
				try {
					reservation.ajouterReservation(mainVue.getClientName());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				try {
					reservation.ajouterReservation(mainVue.getClientName());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
	private class AfficherListe implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			reservation.semaineCourante = mainVue.getRadioState();
			vueReservation.contentPane.setVisible(true);
			mainVue.frame.setVisible(false);
			
			if (reservation.semaineCourante) {
				
			}
		}
	}
}

