package TPInterface.View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;

public class vueReservation extends JFrame {

	JPanel contentPane;
	DefaultListModel<String> nomsClients = new DefaultListModel<String>();
	private JList<String> listeReservations = new JList<>(nomsClients);
	JButton btnRevenirReserv = new JButton("Revenir aux r\u00E9servations");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vueReservation frame = new vueReservation();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public vueReservation() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 511, 411);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		listeReservations = new JList<String>();
		listeReservations.setBounds(21, 0, 474, 321);
		contentPane.add(listeReservations);
		
		btnRevenirReserv.setBounds(10, 348, 474, 23);
		contentPane.add(btnRevenirReserv);
	}
	
	public void setListe(String nomClient) {
		this.nomsClients.addElement(nomClient);
	}
	
	void btnRevenir(ActionListener listenAddBtn) {
		this.btnRevenirReserv.addActionListener(listenAddBtn);
	}
}
