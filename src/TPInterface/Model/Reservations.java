package TPInterface.Model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import TPInterface3.mainVue.java;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Reservations extends DataBaseEntity {
	final int siegesTotaux = 30;
	private int siegesReserver;
	private int siegesLibres;
	private int siegesReserverProchaine;
	private int siegesLibresProchaine;
	public boolean semaineCourante;
	private String nomFichierProchaine;
	private String messageErreur;
	
	public Reservations(String nomFichier, int ID) throws IOException {
		super(nomFichier, ID);
		this.nomFichierProchaine = "semaineProchaine.txt";
		this.siegesReserver = initSiegesReserver(this.nomFichier);
		this.siegesReserverProchaine = initSiegesReserver(this.nomFichierProchaine);
		this.siegesLibres = this.siegesTotaux - this.siegesReserver;
		this.siegesLibresProchaine = this.siegesTotaux - this.siegesReserverProchaine;
	}
	
	
	public int getSiegesTotaux() {
		
		return this.siegesTotaux;
	}
	
	public int getSiegesRestants() {
		
		return this.siegesReserver;
	}
	
	public int getSiegesLibres() {
		
		return this.siegesLibres;
	}
	
	public int getSiegesReserver() {
		
		return this.siegesReserver;
	}
	
	public String getNomFichier() {
		
		return this.nomFichier;
	}
	
	public String getNomFichierProchaine() {
		
		return this.nomFichierProchaine;
	}
	
	
	public int getSiegesReserverProchaine() {
		
		return this.siegesReserverProchaine;
	}
	
	public int getSiegesLibresProchaine() {
		
		return this.siegesLibresProchaine;
	}
	
	public void setSiegesReserverProchaine(int a_siegesReserverProchaine) {
		
		this.siegesReserverProchaine = a_siegesReserverProchaine;
	}
	
	public void setSiegesLibresProchaine(int a_siegesLibresProchaine) {
		
		this.siegesLibresProchaine = a_siegesLibresProchaine;
	}
	
	public String getMessageErreur() {
		
		return this.messageErreur;
	}
	
	public void setMessageErreur(String a_messageErreur) {
		
		this.messageErreur = a_messageErreur;
	}
	
	public void setSiegesReserver(int a_siegesReserver) {
		this.siegesReserver = a_siegesReserver;
	}
	
	public void setSiegesLibres(int a_siegesLibres) {
		this.siegesLibres = a_siegesLibres;
	}
	
	private int initSiegesReserver(String nomFichier) throws IOException {
		File fichier = new File(this.nomFichier);
		int nbLigne = 0;
		
		if (fichier != null) {
			BufferedReader bufferReader = new BufferedReader(new FileReader(fichier));
			@SuppressWarnings("unused")
			String ligne;
			while ((ligne = bufferReader.readLine()) != null) {
				nbLigne++;
			}
			bufferReader.close();
		}
		
		return nbLigne;
	}
	
	
	@SuppressWarnings("unused")
	public void ajouterReservation(String nomClient) throws IOException {
		File fichier;
		String nomDeFichier;
		int ID;
		
		if (this.semaineCourante) {
			fichier = new File(this.nomFichier);
			nomDeFichier = this.nomFichier;
		} else {
			fichier = new File(this.nomFichierProchaine);
			nomDeFichier = this.nomFichierProchaine;
		}
		
		if (fichier != null) {
			BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(fichier));
			if (getClientID(nomClient) == -1) {
				ID = prochainID(nomDeFichier);
			} else {
				ID = getClientID(nomClient);	
			}
			bufferWriter.append(this.ID + " " + Boolean.toString(this.semaineCourante) + " " + Integer.toString(getClientID(nomClient)) + " " +  nomClient + "\n");
			bufferWriter.close();
			
			if (this.semaineCourante) {
				this.siegesReserver++;
				this.siegesLibres--;
			} else {
				this.siegesReserverProchaine++;
				this.siegesLibresProchaine--;
			}
		
		} else {
			this.messageErreur = "Impossible d'ouvrir le fichier : " + this.nomFichier;
		}
	}
	
	public int getClientID(String nomClient) throws IOException {
		File fichier;
		String ligne;
		int IDClient = -1;
		
		if (this.semaineCourante) {
			fichier = new File(this.nomFichier);
		} else {
			fichier = new File(this.nomFichierProchaine);
		}
		BufferedReader bufferReader = new BufferedReader(new FileReader(fichier));
		
		while ((ligne = bufferReader.readLine()) != null) {
			if (ligne.split(" ")[1] == nomClient) {
				IDClient = Integer.parseInt(ligne.split(" ")[0]);
				break;
			}
		}
		bufferReader.close();
		
		return IDClient;
	}
	
	@SuppressWarnings("unused")
	private void retirerClient(int IDClient) throws IOException {
		File fichier = new File(this.nomFichier);
		File fichierTemporaire = new File("temporaire.txt");
		
		if (fichier != null) {
			BufferedReader bufferReader = new BufferedReader(new FileReader(fichier));
			BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(fichier));
			String ligne;
			
			while ((ligne = bufferReader.readLine()) != null) {
				if (Integer.parseInt(ligne.split(" ")[0]) != IDClient) {
					bufferWriter.append(ligne);
				}
			}
			bufferWriter.close();
			bufferReader.close();
			fichier.delete();
			fichierTemporaire.renameTo(fichier);
			this.siegesReserver--;
			this.siegesLibres++;
		} else {
			this.messageErreur = "Impossible d'ouvrir le fichier : " + this.nomFichier;
		}	
	}
	
	public void resetBaseDonnee() throws IOException {
		PrintWriter printWriter;
		if (this.semaineCourante) {
			printWriter = new PrintWriter(this.nomFichier);
		} else {
			printWriter = new PrintWriter(this.nomFichierProchaine);
		}
		printWriter.close();
	}
	
	public String verifierClient(String nomFichier, int ID) throws IOException {
		File fichier = new File(this.nomFichier);
		String nomClient = "";
		
		if (fichier != null) {
			BufferedReader bufferReader = new BufferedReader(new FileReader(fichier));
			String ligne;
			while((ligne = bufferReader.readLine()) != null) {
				if (Integer.parseInt(ligne.split(" ")[0]) == ID) {
					nomClient = ligne.split(" ")[1];
					break;
				}
			}
			bufferReader.close();
		}
		
		return nomClient;
	}
	
	public int prochainID(String nomFichier) throws IOException {
		File fichier = new File(this.nomFichier);
		int ID = -1;
		String derniereLigne = "";
		
		if (fichier != null) {
			BufferedReader bufferReader = new BufferedReader(new FileReader(fichier));
			String ligne;
			while((ligne = bufferReader.readLine()) != null) {
				derniereLigne = ligne;
			}
			
			if (derniereLigne == "") {
				ID = 0;
			} else {
				ID = Integer.parseInt(derniereLigne.split(" ")[0]) + 1;	
			}
			bufferReader.close();
		}
		
		return ID;
	}
}





















