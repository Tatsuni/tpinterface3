package TPInterface.Model;

import java.io.IOException;

abstract class DataBaseEntity {
	protected String nomFichier;
	protected int ID;
	
	public DataBaseEntity(String nomFichier, int ID) throws IOException {
		this.nomFichier = nomFichier;
		this.ID = ID;
	}
	
	public int getID() {
		
		return this.ID;
	}
	
	public abstract int prochainID(String nomFichier) throws IOException;
	
	public abstract String verifierClient(String nomFichier, int ID) throws IOException;
	
}
