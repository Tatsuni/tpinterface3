package TPInterface.Model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Users extends DataBaseEntity {
	private String nomFamille;
	
	public Users(String nomFamille, int ID, String nomFichier) throws IOException {
		super(nomFichier, ID);
		this.nomFamille = nomFamille;
	}
	
	public void ajouterClient() throws IOException {
		File fichier = new File(this.nomFichier);

		if (fichier != null) {
			BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(fichier));
			bufferWriter.append(this.ID + " " +  this.nomFamille+ "\n");
			bufferWriter.close();
		}
	}
	
	public String verifierClient(String nomFichier, int ID) throws IOException {
		File fichier = new File(this.nomFichier);
		String nomClient = "";
		
		if (fichier != null) {
			BufferedReader bufferReader = new BufferedReader(new FileReader(fichier));
			String ligne;
			while((ligne = bufferReader.readLine()) != null) {
				if (Integer.parseInt(ligne.split(" ")[0]) == ID) {
					nomClient = ligne.split(" ")[1];
					break;
				}
			}
			bufferReader.close();
		}
		
		return nomClient;
	}
	
	public int prochainID(String nomFichier) throws IOException {
		File fichier = new File(this.nomFichier);
		int ID = -1;
		String derniereLigne = "";
		
		if (fichier != null) {
			BufferedReader bufferReader = new BufferedReader(new FileReader(fichier));
			String ligne;
			while((ligne = bufferReader.readLine()) != null) {
				derniereLigne = ligne;
			}
			
			if (derniereLigne == "") {
				ID = 0;
			} else {
				ID = Integer.parseInt(derniereLigne.split(" ")[0]) + 1;	
			}
			bufferReader.close();
		}
		
		return ID;
	}
}
	

